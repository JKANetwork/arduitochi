/* 
 * Arduitochi V0.5.0 by JKA Network for Arduino
 * 
 * Standard pins for components (You can change it in code below)
 * Screen (Nokia 5510 48x84px screen) pins:
 * Serial clock out (SCLK) - 6
 * Serial data out (DIN / DN / MOSI) - 5
 * Data/Command select (D/C) - 4
 * LCD chip select (SCE / CS / CE) - 3
 * LCD reset (RST/reset) - 2
 * 
 * --- Buttons
 * Buttons are pull-up. Connect to GND and pin.
 * Button A - pin 7 - call btnA() function in code
 * Button B - pin 8 - call btnB() function in code
 * Button C - pin 9 - call btnC() function in code
 * ---
 * Sound - pin 12 (change in snd_pin)
 * 
 */

#include <SPI.h> //LCD communication
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <EEPROM.h>
#include "imagenesbmp.h" //File imagenesbmp.h containing image arrays of arduitochi
#define ELEMENTS(x) (sizeof(x) / sizeof(x[0]))

//Arduino pins and screenconfig
const byte snd_pin = 12; //Sound pin
const byte btnA_pin = 7;
const byte btnB_pin = 8;
const byte btnC_pin = 9;
//Screen
Adafruit_PCD8544 display = Adafruit_PCD8544(6,5,4,3,2); //Screen (SCLK,DIN,D/C,CS,RST)
const bool scr_rotate = 1; //If screen is inverted (upside down), change to 0 for view right
const byte scr_contrast = 55; //Screen contrast (0 to 100)
const byte savegame = 0; //1= Auto save game in EEPROM, 0= Not save game (Do not use when testing)
const byte eeprom_shift = 0; //If you want to shift in the EEPROM to not use the first bytes. (Do not use numbers > 254)
//End Arduino config

byte menu[3] = {0,0,0}; //This records where are you (Position of menus and submenus)
byte ingame = 0; //If its playing, this is != 0 and interrupts all normal activities
byte usingobject[2] = {0,0}; //If its using object, this is {1,item}
//Tama position
byte ardupos[3] = {0,0,0}; //Arduitochi position and direction (X,Y,DIRECTION)

byte hungry = 2;
byte happiness = 3;
byte maxstat = 5; //Max hungry-happiness
byte poop = 0; //Has poop?
bool dirty = 0; //If arduitochi doesnt go to bathroom in much time, they makes dirty
unsigned int money = 0;
byte weight = 5; //Weight of ardu
bool items[20]; //Array of 1 or 0. The position of the array is the "object". 1 is bought, 0 or NULL it not.
bool outsideitems[20]; //Like before, but outside items
bool isoutside = 0; //Is ardu outside?

//Constants for decreasing values like hunger (With num is the "limit" for decresing, without it, is the actual number. When var equals numvar,ocurrs
const int numlowerhungry = 1250; 
const int numlowerhappiness = 1600;
const int numdopoop = 1500;
int lowerhappiness = 0;
int lowerhungry = 0;
int dopoop = 0; 
//

//
unsigned long timeevolve; //This have the seconds since last "evolution"
//

int varproc = 0; //Process time counting var (For eating and standby)
bool buyit = 0; //This turns 1 when I want to buy something
byte shopitems[2] = {0,1}; //In shop items
const int shopprices[] = {100,900}; //Prices of the items
byte actual_ardu[3] = {0,1,0}; //The arduitochi you have {stage, tama,sex}
//


byte soundid = 0; //Var for sound playing, if its 0, means none
byte soundstate = 0; //State of the sound. 0 is not playing nothing
byte ralensignal[3] = {0,0,0}; //This is for knowing how much time runs in multitasking. (In SIGNAL(TIMER0_COMPA_vect) works in 10,50,1000ms {10,5,20})
int beenalone = 0; //how much time arduitochi has been alone
bool vardie = 0; //Arduitochi die var
bool issleep = 0; //Is sleeping?

byte counterbtntime = 30; //To avoid "Double-triple clic" with some weird phisical buttons and arduino. In ms time. This counts to 0
byte timerbtntime = 30; //To avoid "Double-triple clic" with some weird phisical buttons and arduino. In ms time. Default var.

//Controls limit of menus
void fun_maxopt(byte &varopt,byte maxopts,byte resetto){
  if (varopt > maxopts){
    varopt = resetto;
  }
}

void set_menu_opts(char **stri,byte elements,byte arrowpos){
  for (byte x =0;x < elements;x++){
    display.setCursor(12, 12+(x*8));
    display.print(stri[x]);
  }
  display.setCursor(0, 12+((arrowpos-1)*8));
  display.print("->");
}


void setup(){

  randomSeed(analogRead(0)); //For random operations

  ADCSRA = 0; //Disable ADC (Disable analog reads after this for energy save. Not used.)
  
  //This makes the interrupt SIGNAL(TIMER0_COMPA_vect) { } every 1 ms, doing some "multitasking"/background process
  OCR0A = 175; //This controls the ms. Seems to be configurable¿
  TIMSK0 |= _BV(OCIE0A);
  //
  
  display.begin();
  /* Setup pull-up buttons */
  pinMode(btnA_pin, INPUT); //btnA
  pinMode(btnB_pin, INPUT); //btnB 
  pinMode(btnC_pin, INPUT); //btnC
  digitalWrite(btnA_pin, HIGH); //Pull up buttons
  digitalWrite(btnB_pin, HIGH);
  digitalWrite(btnC_pin, HIGH);
  /* End setup pull-up buttons */
  
  display.setRotation(scr_rotate * 2); //Rotate display 180 degrees if rotatescr = 1
  display.setContrast(scr_contrast); //Screen contrast
  display.display(); // show adafruit splashscreen
  delay(350);
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0, 0);
  display.print("Arduitochi");
  display.setCursor(0, 8);
  display.print("V0.5.0");
  display.setCursor(0, 16);
  display.print("JKA Network");

  if (EEPROM.read(0) == 0){ //If save not found, found, new game
    display.setCursor(0, 35);
    display.print("New game");
    display.display();
    delay(750);
    display.clearDisplay();
    display.setCursor(12, 10);
    display.print("It's a egg?");
    display.drawBitmap(34, 38, egg_bmp, 16, 10, 1);
    display.display();
    delay(2100);
    display.clearDisplay();
    actual_ardu[2] = random(0,2); //¿It will be a boy or a girl?
    display.setCursor(12, 12);
    if (actual_ardu[2] == 0){display.print("Is a boy!");}else{display.print("Is a girl!");}
    display.drawBitmap(38, 40, tama01_bmp, 16, 8, 1); //16x8
    display.display();
    delay(800);
  }
  else if (EEPROM.read(0+eeprom_shift) != 0){  //If save found
    display.setCursor(0, 35);
    display.print("Loading...");
    display.display();
    eeprom_load();
  }
  delay(600);
}



void loop() {
  if (vardie == 0) { //If it's alive
    if (ingame == 0) {
      display.clearDisplay(); //Erase display
      dibujarpantalla();
      pasodeltiempo();
      if (issleep == 0){
        beenalone++;
      }
      display.display(); //Put updates on display
      evolve(); //It checks if it has to evolve
    }else if(ingame != 0) { //Está jugando.
      display.clearDisplay();
      gameprocess();
      display.display();
    }
    delay(100); //10FPS , limiting program too to 10 loops/sec

  }
  if (vardie == 1) { //If it's dead
     display.clearDisplay();
     display.setCursor(12,4);
     display.print("Arduitochi");
     display.setCursor(18,12);
     display.print("Has died");
     display.drawBitmap(34, 26, grave_bmp, 16, 15, 1); //R.I.P. bmp
     display.display();
     soundid = 10;
     delay(60000);
  }
}

void btnA() {
  beenalone = 0;issleep=0; //You make arduitochi not alone
  
  if (ingame == 0) { //if it's not playing, normal procedures
    if (menu[1] == 0) {
      menu[0]++; //If nothing is selected, continue selecting
      return;
    }
    if (menu[1] != 0 && menu[2] == 0) { //Inside option in menu
      menu[1]++; //Change option for menu[1]
      return;
    }
    if (menu[2] != 0 && menu[0] == 4) { //You entered in shop or game menu
      menu[2]++; //Change option in game/shop menu
      return;
    }
  } //End normal use of buttons

  if (ingame != 0){gamebtn1();}
}

void btnB() {
  beenalone = 0;issleep=0;
  if (ingame == 0) { 
  //Only executes the first coindicence
    if (menu[0] == 4 && menu[1] == 1 && menu[2] != 0) {if (weight >= 6){weight -= 2;varproc = 0;ingame = menu[2];return;}else{soundid=1;return;}} //Load a game
    if (menu[0] == 4 && menu[1] == 2 && menu[2] != 0) {buyit = !buyit;return;} //Buy it
    if (menu[0] == 4 && menu[1] == 3 && menu[2] != 0) {usingobject[0] = 1;return;} //Play with item
    if (menu[0] == 2 && menu[1] != 0 && menu[2] == 1){varproc = 99;return;} //Faster eating
    if (menu[0] == 2 && menu[1] != 0) {menu[2] = 1;varproc = 0;return;} //Making ardu eat (Restore varproc to 0 for avoid bugs)    
    if (menu[0] != 0 && menu[1] == 0) {menu[1] = 1;menu[2] = 0;return;} //If an option is selected (menu[0] != 0)

    //Like "ELSE" (Select to submenu)
    if (menu[0] != 0 && menu[1] != 0) {menu[2] = 1;varproc = 0;return;} //Select into food, bathroom,games,options... I reset varproc for item array proposses

  }

  if (ingame != 0){gamebtn2();}
}

void btnC() {
  beenalone = 0;issleep=0;
  if (usingobject[0] == 1){usingobject[0] = 0;return;} //Stop using objects
  if (ingame == 0){
    if (menu[0] == 0) { //Make something cool
      return;
    }
    if (menu[1] == 0) { //Cancel the "rectangle" over the option
      menu[0] = 0;
      return;
    }
    if (menu[1] != 0 && menu[2] == 0) { //Go back from a menu
      menu[1] = 0;
      return;
    }
    if (menu[0] == 4 && menu[2] != 0) { //Go back from the game/item select
      menu[2] = 0;
      return;
    }
    if (menu[0] == 7 && menu[2] != 0) { //Go back of all in options menu
      menu[2] = 0;
      menu[1] = 0;
      return;
    }
  }

  if (ingame != 0){gamebtn3();}
}

void dibujarpantalla() {
  //Draw menu
  display.drawLine(0, 10, 84, 10, BLACK); //x0,y0,x1,y1 //Separation line

  //Menu icons
  display.drawBitmap(1, 1, ardumenu_status_bmp, 10, 8, 1); //Status
  display.drawBitmap(13, 1, ardumenu_food_bmp, 10, 8, 1); //Eat
  display.drawBitmap(25, 1, ardumenu_cleaning_bmp, 10, 8, 1); //WC
  display.drawBitmap(37, 1, ardumenu_gameshop_bmp, 10, 8, 1); //Games/Shopping/Items
  display.drawBitmap(49, 1, ardumenu_mail_bmp, 10, 8, 1); //Mail
  display.drawBitmap(61, 1, ardumenu_connection_bmp, 10, 8, 1); //Connection
  display.drawBitmap(73, 1, ardumenu_options_bmp, 10, 8, 1); //Options

  //
  fun_maxopt(menu[0],7,0);

  //Draw rectangle menu
  if (menu[0] != 0){
    display.drawRect(12*(menu[0]-1),0,12,10,1);
  }


  if (menu[1] == 0) { //If it's in standby
    if (isoutside == 0){
      ardudraw(ardupos[0], ardupos[1], 1);
      for (byte x = 0; x < poop;x++){
        display.drawBitmap(66-(x*12), 40, poop_bmp, 10, 8, 1); //Draw poop
      }
      if (dirty == 1) {
        display.drawBitmap(ardupos[0]-2, ardupos[1]-3, dirtylines_bmp, 8, 3, 1); //dirty
        display.drawBitmap(ardupos[0]+10, ardupos[1]-3, dirtylines2_bmp, 8, 3, 1); 
      }
    }
   if (isoutside == 1){
    outside_draw();
   }
  }
  if (menu[1] != 0) { //Is inside a menu
    switch (menu[0]) {
      case 1:
        menu_status_draw();
        break;
      case 2:
        menu_meal_draw();
        break;
      case 3:
        menu_wc_draw();
        break;
      case 4:
        menu_gameshop_draw();
        break;
      case 5: //Mail (Not created yet)
        display.setCursor(20, 20);
        display.print("No mail");
        display.setCursor(20,28);
        display.print("for you");
        break;
      case 6: //Connection / codes
        display.setCursor(20, 20);
        display.print("WIP");
        break;
      case 7:
        menu_options_draw(); //Save/Erase
        break;

    }
  }

}

void menu_status_draw() {
  fun_maxopt(menu[1],2,1);
  switch (menu[1]) {
    case 1:
      display.setCursor(4, 12);
      display.print("Hungry:");
      for (byte x = 1; x <= maxstat; x++){
        if (hungry >= x){
          display.print("+");
        }else{
          display.print("-");          
        }
      }
      display.setCursor(4, 22);
      display.print("Happy:");
      for (byte x = 1; x <= maxstat; x++){
        if (happiness >= x){
          display.print("+");
        }else{
          display.print("-");          
        }
      }
      display.setCursor(4,32);
      display.print("Weight: ");
      display.print(weight);
    break;
    case 2:
      display.setCursor(4, 12);
      display.print("Money");
      display.drawBitmap(10, 20, moneybag_bmp, 12, 11, 1); //Money bag
      display.setCursor(25,22);
      display.print(money);
    break;
  }
}

void menu_meal_draw() {
  fun_maxopt(menu[1],2,1);
  if (menu[2] == 0) { //Mientras no ha elegido la comida
    display.setCursor(4, 12);
    display.print("Select food:");
    switch (menu[1]) {
      case 1:
        display.setCursor(4, 22);
        display.print("Soup");
        display.drawBitmap(38, 30, food_soup_bmp, 10, 8, 1);
        break;
      case 2:
        display.setCursor(4, 22);
        display.print("Rice");
        display.drawBitmap(38, 30, food_rice_bmp, 12, 8, 1);
        break;
    }
  }

  if (menu[2] != 0) //Eating
  {
    varproc++;
    display.setCursor(4, 12);
    display.print("Eating");
    ardupos[0] = 15;
    ardupos[1] = 25;
    ardudraw(ardupos[0], ardupos[1], 0);
    dibujocomida();
    if (varproc >= 30) { //El timer... Por no hacer un sleep(ms)
      weight +=2;
      if (hungry <= maxstat){hungry++;}
      varproc = 0;
      menu[2] = 0; //Go back
    }

  }
}

void menu_wc_draw()
{
  if (menu[2] == 0) {
    const char* mys[] = {"Go to WC","Go to Bath"};
    fun_maxopt(menu[1],ELEMENTS(mys),1);
    set_menu_opts(mys,ELEMENTS(mys),menu[1]);


  }
  if (menu[2] == 1)
  {
    switch (menu[1]) {
      case 1:
        if (poop == 0){soundid=1;menu[2] = 0;break;} //If no poop, no WC
        display.setCursor(4, 12);
        display.print("WC (8)");
        //TODO: WC img
        display.display();
        delay(1500);
        poop = 0;
        dopoop = 0;
        menu[1] = 0;
        menu[2] = 0;
        break;
      case 2:
        display.setCursor(4, 12);
        display.print("Shower");
        display.drawFastVLine(12,20,20,BLACK);  //X,Y,DISTANCE
        display.drawFastHLine(12,40,2,BLACK); //X,Y,DIST
        display.drawFastHLine(12,20,8,BLACK); //X,Y,DIST
        display.drawFastHLine(12,21,8,BLACK); //X,Y,DIST
        display.drawBitmap(15, 22, dirtylines2_bmp, 8, 3, 1); //X,Y,DIBUJO,BITSX,BITSY,1.
        display.drawBitmap(15, 25, dirtylines2_bmp, 8, 3, 1); //X,Y,DIBUJO,BITSX,BITSY,1.
        ardudraw(16, 28, 0);
        display.display();
        delay(1500);
        dirty = 0;
        poop = 0;
        dopoop = 0;
        menu[1] = 0;
        menu[2] = 0;
        break;
    }
  }

}

void menu_gameshop_draw()
{
  if (menu[2] == 0) {
    char* mys[] = {"Games","Shop","Items","Go "};
    fun_maxopt(menu[1],ELEMENTS(mys),1);
    set_menu_opts(mys,ELEMENTS(mys),menu[1]);
    display.setCursor(29, 36);
    if (isoutside == 0){ display.print("outside");}else{ display.print("inside");}


  }
  if (menu[2] != 0 && usingobject[0] == 0)
  {
    switch (menu[1]) {
      case 1: //Games
        if (actual_ardu[0] > 0){ //If it's not a baby
          fun_maxopt(menu[2],5,1);
          display.setCursor(12, 36);
          display.print("...");
        }else{ //If it's a baby
          fun_maxopt(menu[2],3,1);
        }
        if (menu[2] < 4){
          char* mys[] = {"Jump","Avoid","Fall"};
          set_menu_opts(mys,ELEMENTS(mys),menu[2]);
        }else{
          char* mys[] = {"Math","Dance!"};
          set_menu_opts(mys,ELEMENTS(mys),((menu[2]+1)%4));

        }
      break;
      case 2: //Shop
      menu_sub_shop_draw();
      break;
      case 3: //Items
        varproc = 0;
       for (byte z = 0;z <= 19;z++){ //Calculate how much objets are buyed
          if (items[z] != 0){varproc++;}
          if (varproc == menu[2]){ //When is the item pos
            item_draw(z,0);
            usingobject[1] = z; //Store item temporaly
            varproc++; //No more coincidences adding 1 to varproc
          }
        }
        if (varproc < menu[2]){menu[2] = 1;} //If it not found anymore objects,reset list
        if (varproc == 0){ //No objets
          display.setCursor(10, 16);
          display.print("No objets");
          display.setCursor(10, 24);
          display.print("Buy some ;)");
        }
        varproc = 0; //For avoiding varproc being infinite
      break;
      case 4:
      isoutside = !isoutside;
      menu[2] = 0;
      menu[1] = 0;
      menu[0] = 0;
      break;
    } 
  }
  if (usingobject[0] == 1){ //Start using object
    using_item_draw();
  }

}


void menu_options_draw()
{
  if (menu[2] == 0) {
    char* mys[] = {"Sound ON","Secret Code","Erase Game","Evolve(DEB)"};
    fun_maxopt(menu[1],ELEMENTS(mys),1);
    set_menu_opts(mys,ELEMENTS(mys),menu[1]);


  }
  if (menu[2] != 0)
  {
    switch (menu[1]) {
      case 2:
        display.setCursor(4, 20);
        display.print("Not yet...");
        display.display();
        break;
      case 3:
        display.setCursor(4, 12);
        display.print("Game erased");
        display.setCursor(4,20);
        display.print("If you did by error");
        display.setCursor(4,28);
        display.print("Save before power off");        
        display.display();
        eeprom_erase();
        delay(4000);
        break;
      case 4:
        timeevolve=9999; //Change this var to make arduitochi evolve
        break;
    }
    menu[2] = 0;
    menu[1] = 0;
    menu[0] = 0;
  }

}

/*
 * pasodeltiempo()
 * This function check status of arduitochi, makes "time" useful making hungry,sad,...
 */
void pasodeltiempo() {
  lowerhungry++;
  lowerhappiness++;
  if (lowerhungry >= numlowerhungry*(actual_ardu[0]+1) && hungry > 0) {
    hungry--;
    lowerhungry = 0;
    if (hungry == 0) {soundid=7;} //Not so many time for die
  }
  
  if (lowerhappiness >= numlowerhappiness*(actual_ardu[0]+1) && happiness > 0) {
    happiness--;
    lowerhappiness = 0;
    if (happiness == 0) {soundid=7;} //Arduitochi is sad
  }
  if (lowerhungry == numlowerhungry*(actual_ardu[0]+1) && hungry == 0) {soundid=7;} //Last advertence
  if (lowerhungry >= (2 * numlowerhungry*(actual_ardu[0]+1)) && hungry == 0) {
    vardie = 1;/*Die*/
  }

  if (dopoop < numdopoop*(actual_ardu[0]+1) && issleep == 0) {dopoop++;} //No caga mientras duerme.
  if (dopoop == numdopoop*(actual_ardu[0]+1) && poop < 3){poop++;dopoop=0;}
  if (poop > 1){dirty = 1;}
  if (beenalone == 250){menu[2] = 0;menu[1] = 0;menu[0] = 0;} //Exit menu if not using
  if (beenalone == 800){eeprom_save();} //Saving before entering Zzz
  if (beenalone >= 800 && hungry > 0){ //If you left arduitochi alone a long time, and it's not near die
    issleep = 1; //Duerme.
    display.fillRect(0,11,84,37,1);
    display.setTextColor(0);
    display.setCursor(50+varproc,28-varproc);
    display.print("Z");
    display.setCursor(56+varproc,24-varproc);
    display.print("z");
    display.setCursor(62+varproc,22-varproc);
    display.print("z");
    display.setTextColor(1);
    varproc++;
    if (varproc > 6){varproc=0;}
    delay(200);//Less FPS
    }
}

void dibujocomida() {
  switch (menu[1]) {
    case 1:
      display.drawBitmap(25, 30, food_soup_bmp, 10, 8, 1);
      break;
    case 2:
      display.drawBitmap(25, 30, food_rice_bmp, 12, 8, 1);
      break;
      //Draw food
  }

}

void ardudraw(byte tamax, byte tamay, byte azar) { //Draw arduitochi
  if (azar == 1) {
    varproc++;
    if (varproc >= 12) { //Change random pos of arduitochi (It can be better xd)
      varproc = 0;
      ardupos[0] = random(15, 55);
      ardupos[1] = random(17, 32);
    }
  }
  if (azar == 2) { //Movimiento horizontal
    varproc++;
    if (varproc >= 9){
      varproc = 0;
    switch (ardupos[2]){
      case 0: //Va a la derecha
      ardupos[0] +=6;
      if (ardupos[0] > 76){ardupos[0] -= 12;ardupos[2] = 1;}
      break;
      case 1: //Va a la izquierda
      ardupos[0] -=6;
      if (ardupos[0] < 16){ardupos[0] += 12;ardupos[2] = 0;}
      }
    }
  }
  //Draw arduitochi. Ardu has a max of 16x10 width. It adds a +Y if is smaller than 10
  switch (actual_ardu[0]) {
    case 0:
      switch(actual_ardu[1]){
        case 1:
        display.drawBitmap(tamax, tamay+2, tama01_bmp, 16, 8, 1); //16x8 . tamay+2 for putting in the "ground" like 16x10 tamas.
        break;
      }
    break;
    case 1:
      switch(actual_ardu[1]){
        case 1:
        if (actual_ardu[2] == 0){ //Sex, male
          display.drawBitmap(tamax, tamay, tama110_bmp, 16, 10, 1);
        }else{ //Female
          display.drawBitmap(tamax, tamay, tama111_bmp, 16, 10, 1);
        }
        break;
      }
    break;
  }
}

void menu_sub_shop_draw(){ //Inside of the shop
  //First, I think that can be random every..1 hour or so, but for the moment, all is displayed
  switch (buyit){
  case 0: //Seeing shop
  if (menu[2] >= 3){menu[2] = 1;}
  item_draw(shopitems[menu[2]-1],1);
  break;
  case 1:  //Buy it
  buyit = 0;
  if (money > shopprices[shopitems[(menu[2]-1)]]) {
    money = money - shopprices[shopitems[(menu[2]-1)]];
    items[menu[2]-1] = 1;
  }
  break;
  }
}

void item_draw(byte item,byte drawprice){ //This draw items and data (shop e item list)
  switch (item){
  case 0:
  display.setCursor(26, 12);
  display.print("Plant");
  display.drawBitmap(34, 22, item_00_icon_bmp, 12, 12, 1);
  break;
  case 1:
  display.setCursor(18, 12);
  display.print("Computer");
  display.drawBitmap(32, 22, item_01_icon_bmp, 16, 12, 1);
  break;
  }
  if (drawprice == 1){
  display.setCursor(32, 36);
  display.print(shopprices[shopitems[(menu[2]-1)]]);
  }
}

void outside_draw(){
  ardudraw(ardupos[0], 32, 2);
  for (byte z = 0;z < 84;z+=5){
    display.drawLine(z,48,z+4,48-4,1);
    display.drawLine(z+2,48,z+4,48-2,1);
    z += 5;
  }
  display.drawBitmap(0,32,outsideitem_00_bmp,16,16,1);
}


SIGNAL(TIMER0_COMPA_vect) { //Hardware interrupt every 1ms
  ralensignal[0]++;
  
  if (ralensignal[0] >= 10){ //Interrupt every 10ms for checking button status. Works great. counterbtntime is for avoidind "Double clic" with some weird phisical buttons
    ralensignal[0] = 0;
    ralensignal[1]++;
    if (counterbtntime == 0){
      if (digitalRead(btnA_pin) == 0){btnA();counterbtntime = timerbtntime;} //A button
      if (digitalRead(btnB_pin) == 0){btnB();counterbtntime = timerbtntime;} //B button
      if (digitalRead(btnC_pin) == 0){btnC();counterbtntime = timerbtntime;} //C button
    }else{
      counterbtntime--;
    }
  }
  
  if (ralensignal[1] >= 5){ //sound "multitask" works every 50ms and not ever 1ms, reducing the load
    ralensignal[1] = 0;
    ralensignal[2]++;
    switch (soundid){
    case 0: //No sound
    //soundstate = 0; //Its zero already, better to only do the break
    break;
    case 1: //'pi' sound
    if (soundstate == 0){tone(snd_pin, 440,100);soundid=0;break;}
    break;
    case 2: //'pi' + 1 score sound
    if (soundstate == 0){tone(snd_pin, 800,60);soundid=0;break;}
    break;
    case 3: //Game Over sound
    if (soundstate == 0){tone(snd_pin, 440);break;}
    if (soundstate == 2){noTone(snd_pin);tone(snd_pin, 420);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 6){noTone(snd_pin);tone(snd_pin, 380);break;}
    if (soundstate == 8){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    case 4: //Poop
    if (soundstate == 0){tone(snd_pin, 440);break;}
    if (soundstate == 2){noTone(snd_pin);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 440);break;}
    if (soundstate == 6){noTone(snd_pin);break;}
    if (soundstate == 8){noTone(snd_pin);tone(snd_pin, 440);break;}
    if (soundstate == 10){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    case 5: //Im alone
    if (soundstate == 0){tone(snd_pin, 400);break;}
    if (soundstate == 1){noTone(snd_pin);break;}
    if (soundstate == 2){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 3){noTone(snd_pin);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 5){noTone(snd_pin);break;}
    if (soundstate == 6){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 7){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    case 6: //Something has occurred
    if (soundstate == 0){tone(snd_pin, 440);break;}
    if (soundstate == 1){noTone(snd_pin);break;}
    if (soundstate == 2){noTone(snd_pin);tone(snd_pin, 440);break;}
    if (soundstate == 3){noTone(snd_pin);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 500);break;}
    if (soundstate == 6){noTone(snd_pin);break;}
    if (soundstate == 7){noTone(snd_pin);tone(snd_pin, 560);break;}
    if (soundstate == 8){noTone(snd_pin);break;}
    if (soundstate == 9){noTone(snd_pin);tone(snd_pin, 560);break;}
    if (soundstate == 10){noTone(snd_pin);break;}
    if (soundstate == 11){noTone(snd_pin);tone(snd_pin, 620);break;}
    if (soundstate == 12){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    case 7: //My life is in danger
    if (soundstate == 0){tone(snd_pin, 400);break;}
    if (soundstate == 1){noTone(snd_pin);break;}
    if (soundstate == 2){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 3){noTone(snd_pin);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 5){noTone(snd_pin);break;}
    if (soundstate == 6){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 7){noTone(snd_pin);break;}
    if (soundstate == 8){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 9){noTone(snd_pin);break;}
    if (soundstate == 10){noTone(snd_pin);tone(snd_pin, 400);break;}
    if (soundstate == 11){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    case 10: //I dead
    if (soundstate == 0){tone(snd_pin, 200);break;}
    if (soundstate == 4){noTone(snd_pin);tone(snd_pin, 180);break;}
    if (soundstate == 6){noTone(snd_pin);tone(snd_pin, 160);break;}
    if (soundstate == 8){noTone(snd_pin);tone(snd_pin, 140);break;}
    if (soundstate == 10){noTone(snd_pin);soundstate = 0;soundid=0;break;}
    break;
    }
    if (soundid != 0){soundstate++;} //For doing sounds, this go forward in the sound time
  }
  if (ralensignal[2] == 20){ //1 sec.
    ralensignal[2] = 0;
    timeevolve++; //Every sec, update it
  }
}

void evolve(){
   switch (actual_ardu[0]){
    case 0:
      if (timeevolve > 130){ //130 sec min for evolving
        timeevolve = 0;
        evolveproc();
        actual_ardu[0]++;
      }
   }
}

void evolveproc(){
  for (varproc = 0;varproc < 2;varproc++){
  display.clearDisplay();
  display.setCursor(0,12);
  display.print("Evolving...!");
  ardudraw(38,38,0); 
  display.display();
  delay(1500);
  display.clearDisplay();
  display.setCursor(0,12);
  display.print("Evolving...!");
  display.drawRect(34, 34, 16, 12, 1);
  display.fillRect(34, 34, 16, 12, 1);
  display.display();
  delay(1500);
  }
}

void eeprom_load(){
  /* EEPROM map
   * 0 - Version
   * 1 - Hungry
   * 2 - Happiness
   * 3,4,5,6 - Money (2 position int, I keep 4 positions for be sure that it will not be replaced)
   * 7 - Weight
   * 8 - Age
   * 50 to 90 - Items (Yes, the rest is 'free',but is better to reserve free space)
   * 90 to 120 - House/ever visible items
   */
  EEPROM.get(1+eeprom_shift,hungry);
  EEPROM.get(2+eeprom_shift,happiness);
  EEPROM.get(3+eeprom_shift, money);
  EEPROM.get(7+eeprom_shift, weight);
  EEPROM.get(50+eeprom_shift, items); //From 50 to 90 
  EEPROM.get(90+eeprom_shift, outsideitems);
}

void eeprom_erase(){
  EEPROM.write(0+eeprom_shift,0); //Version to 0 makes aruitochi think that there is no game. There is no win in erasing all EEPROM and uses write cycles.
}

void eeprom_save(){
  if (savegame == 1){ //Only save if option is enabled.
    EEPROM.put(0+eeprom_shift,1); //Version
    EEPROM.put(1+eeprom_shift,hungry);
    EEPROM.put(2+eeprom_shift,happiness);
    EEPROM.put(3+eeprom_shift, money); //2 bytes int (I leave 4 in total just in case)
    EEPROM.put(7+eeprom_shift, weight);
    EEPROM.put(50+eeprom_shift, items);
    EEPROM.put(90+eeprom_shift, outsideitems);
  }

}
